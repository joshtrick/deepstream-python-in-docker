#!/bin/bash

DIR_PATH=$(dirname $(realpath $0))

apt update
apt install -y \
      autoconf \
      libtool \
      python-gi-dev \
      python3-dev \
      libgstreamer1.0-dev \
      libgstrtspserver-1.0-0 \
      gstreamer1.0-rtsp \
      libgirepository1.0-dev \
      gobject-introspection \
      gir1.2-gst-rtsp-server-1.0

export GST_LIBS="-lgstreamer-1.0 -lgobject-2.0 -lglib-2.0"
export GST_CFLAGS="-pthread -I/usr/include/gstreamer-1.0 -I/usr/include/glib-2.0 -I/usr/lib/x86_64-linux-gnu/glib-2.0/include"

cd $DIR_PATH/..
tar xvf gst-python.tar
cd gst-python

git checkout 1a8f48a
./autogen.sh PYTHON=python3
./configure PYTHON=python3
make
make install

cd /opt/nvidia/deepstream/deepstream/lib
python3 setup.py install

# mkdir -p /opt/nvidia/deepstream/deepstream/sources
# cd /opt/nvidia/deepstream/deepstream/sources
# git clone https://github.com/NVIDIA-AI-IOT/deepstream_python_apps
