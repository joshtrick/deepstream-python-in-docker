sudo docker run \
  --rm \
  --runtime nvidia \
  --network host \
  -v /home/joshtrick/workspace:/root/workspace \
  -w /root \
  -it \
  nvcr.io/nvidia/deepstream-l4t:5.1-21.02-samples \
  bash
