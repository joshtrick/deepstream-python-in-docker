#!/bin/bash

if [[ "$1" == "" ]]
then
  exit
fi

sudo chgrp -R $(id -g) $1
sudo chown -R $(id -u) $1
sudo find $1 -type d -exec chmod 755 {} \;
sudo find $1 -type f -exec chmod 644 {} \;
