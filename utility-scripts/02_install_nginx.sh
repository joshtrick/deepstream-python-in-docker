#!/bin/bash

DIR_PATH=$(dirname $(realpath $0))

apt update
apt install -y \
      libpcre3 \
      libpcre3-dev \
      openssl \
      libssl-dev \
      zlib1g-dev

cd $DIR_PATH/..
cd nginx/nginx-1.18.0

./configure --add-module=../nginx-http-flv-module-master
make
make install
cd ..
cp /usr/local/nginx/conf/nginx.conf /usr/local/nginx/conf/nginx.conf.bak
cp nginx.conf /usr/local/nginx/conf/
/usr/local/nginx/sbin/nginx

