# Deepstream Python in Docker

## Table of Contents
- [Basic Info](#Basic Info)
- [nginx Basics](#nginx Basics)
    - [Commands](#Commands)
    - [Configurations](#Configurations)
    - [Streaming](#Streaming)

<a name="Basic Info"></a>
## Basic Info

|Item|Information|
|---|---|
|Jetson Nano OS Image   |jetson-nano-jp451-sd-card-image.zip            |
|Deepstream Docker Image|nvcr.io/nvidia/deepstream-l4t:5.1-21.02-base   |
|                       |nvcr.io/nvidia/deepstream-l4t:5.1-21.02-iot    |
|                       |nvcr.io/nvidia/deepstream-l4t:5.1-21.02-samples|

<a name="nginx Basics"></a>
## nginx Basics

<a name="Commands"></a>
### Commands
```
# Start nginx
/usr/local/nginx/sbin/nginx

# Check nginx Configurations
/usr/local/nginx/sbin/nginx -t

# Reload nginx
/usr/local/nginx/sbin/nginx -s reload
```

<a name="Configurations"></a>
### Configurations
HTTP-FLV Configurations:
```
    http {
        ...
        server {
            listen 8081; #not default port 80
            ...

            location /flv {
                flv_live on;
            }
        }
    }
```

HLS Configurations:
```
    http {
        ...
        server {
            listen 8081; #not default port 80
            ...

            location /hls {
                root /tmp/;
            }
        }
    }
```

RTMP Configurations:
```
    rtmp {
        ...
        server {
            listen 1935;
            ...

            application myapp {
                live on;
                hls on;
                hls_path /tmp/hls/;
            }
        }
    }
```

<a name="Streaming"></a>
### Streaming
```
# via RTMP
rtmp://example.com[:port]/appname/streamname

# via HTTP-FLV
http://example.com[:port]/dir?[port=xxx&]app=appname&stream=streamname

# via HLS
http://example.com[:port]/dir/streamname.m3u8
```
